# Vos dossiers Dropbox

![](/images/dropbox.jpg)

## Dropbox

Un dossier personnel va être créé sur Dropbox pour chacun et chacune d'entre vous. Ces dossiers ne seront partagés qu'entre vous et moi (2 personnes). Ce sont ces dossiers qui serviront de référence pour vos cotations hebdomadaires.

Les fichiers de vos exercices devront donc être déposés dans ces dossiers Dropbox la veille du cours **\(pas de partage de fichier – uniquement dépôt dans votre dossier\)**. 

**Une fois votre fichier uploadé, laissez-le dans le dossier**, et conservez-en une copie sur un de vos disques. S’agissant d’un dossier partagé, si vous déplacez un fichier hors de ce dossier, je n’y aurai plus accès. Il sera donc noté comme non rendu.

**N’enregistrez dans ce dossier QUE des fichiers qui concernent notre cours.** 
Tout autre fichier serait supprimé. Ce dossier n’est partagé qu’entre vous et moi, aucune autre personne n’y a accès.

Vous êtes responsable de l’ensemble de vos fichiers et exercices, même après remise.
**Conservez donc toujours une sauvegarde** des exercices réalisés sur votre disque dur ou sur une clé USB.

Conservez également vos informations de connexion à Dropbox \(que vous entrez lors de votre inscription\), afin d’être toujours en mesure d’accéder à ce dossier durant l’année. Si vous téléchargez et installez Dropbox \(ce qui n’est pas obligatoire\), le dossier partagé apparaîtra dans la liste de vos dossiers, sur votre ordinateur.

> Plus d'infos sur [le site de Dropbox](https://www.dropbox.com/)
