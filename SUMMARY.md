# Summary

## Informations générales

* [Introduction](README.md)
* [Digitalab \| le site du CASO](http://www.digitalab.be/)
* [Formulaire d'inscription](formulaire.md)
* [Dropbox](dropbox.md)
* [Exercices](exercices.md)
* [Remises de fin d'année](mise-en-page/foldunfold.md)
  
## Dessin vectoriel

* [Les formes simples](vectoriel/formessimples.md)
* [Groupes & alignements](vectoriel/alignements.md)
* [Dessin à la plume](vectoriel/plume.md)
* [The Shapes Project](vectoriel/the-shapes-project.md)
* [Pathfinder, masques & texte](vectoriel/pathfinder-et-masques.md)
* [Motifs](vectoriel/motifs.md)


## Images bitmap

* [Sélections et calques](bitmap/selections.md)
* [Masques de fusion](bitmap/masques.md)
* [Calques de réglages 1 & 2](bitmap/reglages.md)
* [Outils de retouche](bitmap/retouches.md)

## Mise en page

* [Gabarits, pages et blocs](mise-en-page/gabarits.md)
* [Habillages, images et exports](mise-en-page/images.md)
