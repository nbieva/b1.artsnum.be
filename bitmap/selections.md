[![Villa del Casale, Sicile](/assets/villa-del-casale.png)](https://youtu.be/MMkRxatxyWc?t=415)

#[Wine dramas for a pixel](https://youtu.be/MMkRxatxyWc?t=415)

{{ "https://www.youtube.com/watch?v=axTPnSd5f3M" | video }}
[Place](https://en.wikipedia.org/wiki/Place_(Reddit), Reddit.

> There is an empty canvas.
You may place a tile upon it, but you must wait to place another.
Individually you can create something.
Together you can create something more.

https://www.youtube.com/watch?time_continue=167&v=0QO0yZldC2M

[Place / The Atlas](https://draemm.li/various/place-atlas/)

+ Réception des cartographies
+ KIKK festival, GROW Paris
+ Images matricielles (bitmap)
+ [Bits](https://fr.wikipedia.org/wiki/Octet) & maps
+ **définition, résolution, profondeur**
+ Filtres Google et images libres
+ Formats de fichier (RAW, JPG, PNG, GIF, TIFF)

![Carl Andre](/assets/andreppp.png)

![256](/assets/256.jpg)

# Les sélections

+ Dropbox et Photoshop
+ Les calques dans Photoshop (Nouveau, dupliquer, opacité, groupes..)
+ Import d'image + Objets dynamiques
+ Principe de **non-destructivité** des images, règles de base
+ Principaux outils de sélection
+ La plume comme outil de sélection
+ Enregistrer une sélection, inverser une sélection, transformer une sélection
+ Utilisation du pinceau, modification des brosses


+ https://commons.wikimedia.org/wiki/File:Orion_Nebula_-_Hubble_2006_mosaic_18000.jpg?uselang=fr
+ https://fr.wikipedia.org/wiki/Image_num%C3%A9rique#D%C3%A9finition_et_r%C3%A9solution