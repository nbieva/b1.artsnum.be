# Réglages & retouches

![Jacques Perconte](/assets/perconte.png)

##Retour sur les calques de réglages

![Anne-Laure Etienne](/assets/annelaureetienne.png)

> http://www.unspokenimage.com/untitled-gallery#6

http://www.unspokenimage.com/untitled-gallery#5 (Eden Millon)

##Nous savons:

- Ce que sont résolution, définition et taille d'image.
- Le fonctionnement des calques dans Photoshop
- Transformer un arrière-plan en calque
- Renommer un calque / grouper des calques (+ **aplatir une image**)
- Importer/copier une image dans un document existant (+objets dynamiques).
- Utiliser les outils de sélection de base (Sélection rectangulaire, lasso, etc..)
- Créer un tracé vectoriel et le transormer en sélection (ou inversément)
- Ajouter et utiliser un masque de fusion
- Ajouter et utiliser un calque de réglage
- Ajouter et utiliser un calque de remplissage

##Aujourd'hui

- Travaux rendus (+ mises au point)
- Outils de retouche: tampon de duplication
- Outil correcteur et correcteur localisé
- Outil Pièce

##A faire au cours

![Retouche](/assets/retouche1.jpg)
![Retouche](/assets/retouche2.jpg)

- Nettoyer une image
- Dupliquer une fenêtre
- Effacer quelqu'un d'une image
- Filtre fluidité
- Exercice récapitulatif Georges Rousse

[Anthony Gormley, photo originale](/assets/gormley01.jpg)

![Anthony Gormley](/assets/gormley02.jpg)

![Retouches](/assets/retouches02.jpg)

![Retouches](/assets/retouches01.jpg)

![Retouches](/assets/retouches03.jpg)

![India_Lawton](/assets/2013_03_27_India_Lawton_16.jpg)

[Façade 01](/assets/facade01.jpg)
[Façade 01](/assets/facade02.jpg)

##Architectures

- Philippe de Gobert

- Filip Dujardin

##[Walter de Maria](/assets/02_walterDeMaria.zip)##

![Résultat](/assets/RESULTAT.jpg)
