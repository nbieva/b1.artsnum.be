![Eliasson](/assets/reglages02.jpg)
Olafur Eliasson
/ Room for one colour, 1997
/ Moderna Museet, Stockholm 2015
/ Photo: Anders Sune Berg

# Calques de réglages

+ Retour sur les exercices et cartos.
+ Retour sur les masques de fusion + [Performing for the camera](http://www.digitalab.be/performing-for-the-camera/) (le catalogue est à la bibliothèque)
+ La sélection par couche (L'Empire des lumières)
+ Eclaircir ou assombrir les tons clairs/moyens/foncés
+ Les réglages via Image > Réglages
+ Calques de **réglages** et calques de **remplissage**
+ Appliquer un calque de réglage au calque inférieur.
+ Passer du masque de fusion au calque de réglage: faire une seule fois le travail (!).
+ Niveaux, exposition...
+ Teinte/Saturation > Cibler une couleur particulière
+ Calques de remplissage
+ Modes de fusion
+ Masques associés
+ "Peindre" un réglage. (Visages)
+ Outils Densité- et densité+

https://www.reddit.com/r/pics/comments/qefxw/a_local_chemical_plant_caught_fire_last_night/


**MUR OLAFUR !!**

1ère catégorie: modification des niveaux de gris + noir et blanc (contraste, exposition..)
2e catégorie: modification des couleurs (teinte/saturation, balance des couleurs..)

##Exercice au cours:

+ Green river
+ Moss wall Eliasson
+ Empire des lumières
+ Transformer une sélection.

##Pour le 30/11:

Partez de l'image de l'Empire des lumières ci-dessous et remplacez le ciel par une autre image de votre choix (créez quelque chose d'intéressant...) en utilisant les masques de fusion et les outils vus au cours. Déposez le fichier .psd dans votre Dropbox avant le prochain cours.

![empiredeslumieres](/assets/empiredeslumieres.jpg)

![Eliasson](/assets/turell.jpg)


![Lucien](/assets/lucien1.png)
Lucien Schubert

![Lucien](/assets/lucien2.png)
![Lucien](/assets/lucien3.png)


[Eliasson](/assets/base-eliasson.jpg), [Beecroft](/assets/base-beecroft.jpg), [Résultat](/assets/base-resultat.jpg)

[Marylin](/assets/marylin.jpg)

##A faire au cours

![Yayoi Kusama](/assets/kusama.jpg)

![Yayoi Kusama](/assets/masques.png)

![Yayoi Kusama](/assets/kusama2.png)

##Pour le 07/12:

![Christo](/assets/christo-01.jpg)

![Christo](/assets/christo-02.jpg)