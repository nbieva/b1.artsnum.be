# Les masques de fusion

+ Retour sur les sélections
+ Sélection par plage de couleurs
+ Les masques de fusion : principes de base
+ Création avec sélection active
+ Duplication de masques

![magritte](/assets/kanghee01.png)
> Via Alex Jo, [LCANP1819](https://www.facebook.com/groups/1735404680088652)

[Photo 1](/assets/kanghee02.png), [Photo 2](/assets/kanghee03.png)

## Pour le prochain cours (à la rentrée)

Sur le principe du travail de Kim KangHee (voir ci-dessus et sur son site), réalisez une combinaison de minimum 3 images.
+ Ce travail doit obligatoirement inclure une ou plusieurs sélections complexes (type nuage ou fumée) et l'usage de masques de fusion.
+ Votre format fini doit être de 100x150mm (orientation libre) à 300 ppp, mode colorimétrique RVB.
+ Doivent être remis dans votre dossier Dropbox, avant le prochain cours, votre fichier photoshop (.psd) et un fichier JPG.
Veillez à la bonne organisation de vos calques ainsi qu’à la qualité des images utilisées!

ATTENTION: travail non-destructif! (D'où l'utilisation des masques..)


-----

Alternativement, vous pouvez réalisez le travail suivant:

##Golconde, 1956

Sur base du fichier ci-dessous, créez une nouvelle image en partant de nouveaux visuels.
Il doit cependant obligatoirement s’agir d’une architecture devant laquelle (ou sur laquelle) tombent une série d’éléments. Vous devrez utiliser pour ce faire, entre autres: calques, **masques de fusion**, transparences, duplication, transformations, symétrie, modification d’échelle, détourage à l’aide des masques.
Veillez à la bonne organisation de vos calques ainsi qu’à la qualité des images utilisées! Le mieux est souvent d’utiliser vos propres images…

Format du fichier:
100x150mm, 300ppp, mode colorimétrique RVB.
Le fichier est à me rendre à la fois au format .psd et au format .jpg (exportation pour le web, avec un taux de compression ajusté)

![magritte](/assets/golconde.jpg)
![magritte](/assets/_7.jpg)
![magritte](/assets/_20.jpg)
![magritte](/assets/_32.jpg)
![magritte](/assets/_38.jpg)