# Masques d'écrêtage + Texte

+ Retour sur workshop, cartographie + Pathfinder
+ Le **texte** dans Illustrator
+ [Google WebFonts](https://fonts.google.com/): aussi pour votre ordinateur (Comment installer une police)
+ Les **masques d'écrêtage**: principes de base
+ [Josh Brill](https://www.google.be/search?q=josh+brill&tbm=isch&tbo=u&source=univ&sa=X&ved=2ahUKEwjAxsnH5eLdAhXEbFAKHfVMDpsQsAR6BAgGEAE&biw=2093&bih=1043#imgrc=Iopz45zk-aw1IM:) + masque

##Mais aussi..

+ Dégradés de formes
+ Formes personnalisées

![Josh](/assets/Black-cappedChickadee_JoshBrill.jpg)
