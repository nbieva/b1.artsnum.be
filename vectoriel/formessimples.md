# Les formes simples

![Stokholm Design Lab](/assets/flags.gif)[**Stokholm Design Lab**](http://www.stockholmdesignlab.se/venice-biennale/) - Fare mondi, 53e Biennale de Venise, 2009

Fare mondi \(Faire les mondes\), se positionne à contre courant des effets médiatiques du marché de l’art. l’exposition invoque le recours à la poésie, à l’utopie nécessaire à toute invention et à la mise en avant du processus de création des artistes. Est appelée une partie de lavant-garde historique mondiale, le roumain Cadéré, les brésiliens Lygia Pape et Cild Meireles, la poésie japonaise avec Gutaï et Yoko Ono, le suédois Jan Hafström ou encore l’italien Pistoletto A côté de ces figures tutélaires se déploie une jeune génération venue de tous horizons qui architecture, met en réseaux, tisse des liens universels par delà le principe d’identité. On peut ainsi explorer un kaléidoscope de mondes différents : les jeux de tension de l’installation du chinois Jianxi Chin, dinquiétants théâtres intimes à travers le film de l’israélienne Keren Cyter, les méandres du processus mental de la française Dominique Gonzales Foerster jusquaux visions spatiales et métaphoriques de l’italienne Grazia Toderi…

[**Nina Rodrigues-Ely**](http://www.observatoire-art-contemporain.com/revue_decryptage/contributeurs_permanents.php#contributeur10)
Publié le 20/09/2009

---

+ [Les courbes de Bézier ont redessiné le monde](http://paris.blog.lemonde.fr/2007/09/15/les-courbes-de-pierre-bezier-ont-redessine-le-monde/)

## Au programme

* Formulaire
* Agenda et fonctionnement (calendrier, Dropbox, remises..)
* Alternatives à Illustrator et [logiciels libres](logiciels-libres.md) \([Inkskape](https://inkscape.org/fr/), The [Gimp](https://www.gimp.org/fr/), [Scribus](https://www.scribus.net/), [OpenStreet Map](https://www.openstreetmap.org/#map=18/50.81793/4.37322), ..\) + [FLOSS manuals](https://www.flossmanualsfr.net/)
* [Vectoriel/bitmap](https://www.google.be/search?q=vectoriel+bitmap&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiGu6nR18vdAhUCzaQKHeE7DRIQ_AUICigB&biw=1440&bih=718#imgrc=5MPVe11W3lezuM:)
* L’environnement de travail
* Nouveau document + options
* Modes colorimétriques ([RVB/CMYK](https://www.google.be/search?q=cmyk+rgb&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiDxprX18vdAhUQy6QKHS4NAzoQ_AUICigB&biw=1440&bih=718#imgrc=CkgfhxIYlRmTzM:))
* Les fenêtres/panneaux \(+comment les manipuler\)
* La barre d’options
* Repères et repères commentés
* Préférences et unités.
* Notion de bord perdu
* Plans de travail multiples
* Une forme simple
* Transformation d'objets
* Principaux raccourcis clavier
* Contour et remplissage / Intervertir
* Fenêtre des calques (et sélection)
* Disposition avant/arrière
* Verrouiller/Dupliquer/masquer
* Utilisation d'une image comme modèle (verrouiller, modifier l'opacité, passer à l'arrière-plan)

Dans un premier temps,  enregistrez vos fichiers au format .ai (Illustrator) ou .svg (si vous tavaillez avec Inkscape).

## Exercice HORS cours
A rendre **avant** le prochain cours.

1. Dans l'image ci-dessous (Matt Mullican), **choisissez 3 signes** que vous reproduisez à l’aide des formes simples vues au cours. **Inventez-en un quatrième**. N'utilisez que du noir et du blanc. Votre document Illustrator doit donc comprendre **4 plans de travail de 10x10cm** \(avec un fond perdu si nécessaire\).

2. Configuration de votre dossier Dropbox et transfert de votre fichier Illustrator dans votre dossier personnel (nommez vos fichiers comme suit **nom-prenom-atelier-mullican.ai**)

![](/assets/mullican1.jpg)

---

## Matt Mullican  

Matt Mullican est un artiste californien dont l’œuvre se divise en deux grandes parties. D’un côté, des modèles cosmologiques, des mondes réinventés dans une logique post-conceptuelle, avec des systèmes de symboles et de signes empruntés ou créés, et de l’autre côté, un travail lié à l’hypnose \(bien que les deux facettes ne soient pas antithétiques comme l’a montré l’exposition de l’Institut d’art contemporain,\_12 BY 2, \_en 2010\). La première entité fait appel à des logos, schémas et à des notions fondamentales et symboliques que Matt Mullican met en scène dans des dessins, maquettes, cartes, vidéos – il sera d’ailleurs un pionnier de la réalité virtuelle.

{{ "https://www.youtube.com/watch?v=OXwIfAnV9yk" | video }}

_The IAC Mural, 15 June 2010_, gigantesque fresque couvrant le mur jouxtant l’IAC illustre ainsi les cinq niveaux de la cosmogonie de Mullican : au vert correspond l’essentiel, les éléments, la mort, au bleu le mystère de l’inconscient \(_the world unframed_\), au jaune les manifestations conscientes \(_the world framed_\), au noir, le langage et au rouge le spirituel, les idées, le paradis. L’autre aspect de son travail est inauguré par des performances théâtrales à partir des années 1970, dans lesquelles, hypnotisé, il se projette dans des images puis fait le récit de son expérience. Lui qui a passé son enfance à Rome commence avec une gravure de Piranèse, dans laquelle il se promène, passant au delà du visible. Il a de plus en plus recours à l’inconscient, utilisant des acteurs puis se mettant exclusivement en scène sous hypnose. C’est alors qu’apparaît malgré lui une autre personnalité. Tandis qu’il est hypnotisé, il réalise des œuvres, des calligraphies, des peintures, des écritures automatiques. Pour Mullican, l’auteur de ces œuvres est un autre accessible sous hypnose, ni un homme, ni une femme, un autre à l’identité multiple qu’il appelle toujours « That Person ».

En 2005, le musée Ludwig de Cologne commande ainsi une exposition à That Person et non à Matt Mullican. À l’IAC en 2010, c’est la rencontre des œuvres des deux qui est orchestrée dans les 12 salles comme le suggère le titre_12 BY 2_, l’exposition rassemblant 40 ans de création de Matt Mullican et de son double. Dans ces œuvres complexes, et sur un mode qui n’est pas sans rappeler celui des Surréalistes, Mullican travaille et joue en permanence avec notre perception du monde et de ses codes.

Source : [Institut d’art contemporain de Villeurbanne](http://i-ac.eu/fr/artistes/98_matt-mullican)

[Un lien audio](http://www.moma.org/explore/multimedia/audios/391/6832) où Matt Mullican explique la genèse de son travail \(Eng\)

[Un autre…](http://www.poptronics.fr/Matt-Mullican-portrait-de-l)

## Autres...
 
[Image 1](/assets/bauhaus1.jpg),[Image 2](/assets/bauhaus2.jpg),[Image 3](/assets/bauhaus3.jpg) ou [Image 4](/assets/bauhaus4.jpg)
