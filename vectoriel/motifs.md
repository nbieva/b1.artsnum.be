# Les motifs

![](/assets/viallat.jpg)

+ Retour sur la cartographie
+ Les tracés transparents
+ Principe des motifs
+ Dégradés de couleurs et filets de dégradés

+ Principe des motifs
+ Point d'origine des motifs
+ Créer un motif
+ Modifier un motif
+ Création d'un motif simple, sans raccordement

+ https://amandabaum.com/TEXTILE-PATTERNS

![](/assets/motif10.png)

##Marble floor

A partir du travail *Marble floor* (1999) de Wim Delvoye:

1. Recréez un des deux motifs repris ci-dessous. 
2. A l'aide des masques d'écrêtage, modifiez-le pour en faire une version 'Charcuterie'

![](/assets/delvoye1.jpg)
![](/assets/delvoye2.jpg)

----

##Vuittami

A l'instar de ![Takashi Murakami](https://www.google.be/search?rlz=1C5CHFA_enBE752BE753&biw=1309&bih=653&tbm=isch&sa=1&ei=ulzAW6nYKtDVwQKNobLwDw&q=takashi+murakami&oq=tamurakami&gs_l=img.3.0.0i7i30k1l10.7763.14390.0.16216.8.6.2.0.0.0.81.373.6.6.0....0...1c.1.64.img..0.8.382...0j35i39k1j0i67k1j0i30k1j0i8i7i30k1.0.SqBT5aeXmqY), créez votre version personnelle du motif Louis Vuitton, à partir du document ci-dessous.

![](/assets/vuitton.png)
![](/assets/murakami.jpg)

Dans les deux cas, il vous faudra être méthodiques et précis (Alignements, groupes, pathfinder, etc..)
Veillez à sauver régulièrement votre travail.

-----

Au final, votre image ne doit comporter que 3 objets:

+ L'appareil photo
+ Le cercle blanc
+ Le fond jaune

![](/assets/photo1.png)
![](/assets/photo2.png)
![](/assets/photo3.png)

---- 

+ [Exercice alignements](/assets/alignements.pdf)

![](/assets/dominos.png)