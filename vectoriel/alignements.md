# Groupes, alignements, pathfinder

![](/assets/kelly.jpg)

####Retour sur le cours précédent

Nous savons:

+ Créer des **formes simples**
+ Modifier **Remplissage et contour**
+ Appliquer une **transformation** simple (modifier l'échelle ou l'orientation)
+ Organiser les **calques**
+ Importer une **images**

#### **Raccourcis utiles**

+ **Cmd + Alt + déplacement** : dupliquer/placer
+ **Cmd + D** : dupliquer la dernière transformation  
+ **Cmd + G** : grouper \(associer\)
+ **Shift + Cmd + G** : dégrouper \(dissocier\)
+ et bien sûr **Cmd + S** : Sauvegarder

#### Au programme

+ Manipulation de points d'ancrages (et segments)
+ Association/Dissociation d'objets (**Grouper/dégrouper**)
* Les **alignements**
* Distribution/répartition d'objets
* Utiliser les limites d'aperçu
* Rappel **Cmd Y**
* Pathfinder


![](/assets/99316D13-31EB-4099-85E1-171BD1B074DD.png)

![](/assets/Ikko-Tanaka.jpg)