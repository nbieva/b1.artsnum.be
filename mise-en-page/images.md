# Images, habillages de texte et exports

- Exports PDF
- Couleurs
- Gabarits et layouts
- Images
- Assemblage

Retour sur les styles et gabarits
Insertion d'images
Habillages de texte
Numérotation automatique
Trouver et installer des polices
Couleur et nuancier
Dégradé progressif
Exports PDF
Assemblage Indesign
Au cours:
Réalisez travail ci-dessous avec les infos ci-dessous:
Le PDF comme guide: http://mousehover.net/indd/mon.pdf Les images: http://mousehover.net/indd/Images.zip
Infos:
Un gabarit pour la double page + numérotation automatique
Document A4, 6 pages
Marges et colonnes: 15mm, sauf marge de pied (25mm). 3 colonnes, Gouttière: 3mm.
Nom de l’artiste en page 1: Arial bold, 48pt + vectoriser + import de photo dans le tracé.
Styles de paragraphe:
Paragraphe standard: Arial Regular, 11pt, Interligne 14pt, Espace après: 3mm, Couleur: noir 90%
Introduction: 15pt, Interligne: 20pt, Couleur: Cyan 100%
Nom de l’artiste (pg 2): Arial Ragular, 24pt, Interligne 24pt, Espace avant: 11mm, Espace après: 5mm, Couleur: Cyan 100%
Styles de caractère:
Bleu: Arial Italic, Soulignement (Graisse: 0.5pt, Décalage: 3pt)
Texte:
« Art as art » devient plus ou moins la devise de cet artiste qui utilisera la littéralité de ses œuvres pour tenter de penser les choses de manière objective, les pièces souvent tautologiques nous disent qu’elles ne pourraient être autrement que comment elles sont: Five Words In Orange Neon se compose de cinq néons orange et nous indique de ne pas regarder autre chose que ce que l’on voit, en évitant toute inter- prétation pourtant tentée ici.
