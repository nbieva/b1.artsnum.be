#Gabarits, pages, blocs et styles

- Remises des travaux bitmap
- Point sur la fin de quadrimestre
- Horaires des premières lectures **Fold/Unfold**, 11 ou 18 janvier.

- Travail que l'on fait au cours et qu'il faut me rendre à la fin du prochain cours.

![](/assets/assemblage02.png)

##Scribus

- https://scribus.fr/
- [La doc sur Floss Manuals FR](https://fr.flossmanuals.net/initiation-a-scribus/esquissez-votre-document/)

![](/assets/scribus01.jpg)

##Interface

- Espace de travail
- Marges, colonnes et grille

##Principe de l'assemblage

- [Préparer ses sources](https://fr.flossmanuals.net/scribus/preparer-les-sources/)
- Assemblage

##Gabarits et pages

- Le panneau **Pages** ([Avec INDD](https://helpx.adobe.com/be_fr/indesign/using/pages-spreads-1.html). Avec Scribus.)
- Principe des **gabarits**
- Les calques
- Numérotation automatique

##Le texte

- **Blocs de texte**s et colonnes + **tracés** à la plume
- Texte de substitution
- **Styles** de paragraphes et de caractères
- Le texte en excès
- **Chaînages** de texte
- **Alignements** (sur la sélection, la page, les marges..)
- Trouver et installer des **polices**

##Les images

- Les blocs d'**images** + tracés à la plume
- Imports d'images + imports multiples(dans les blocs ou à la volée)
- Les performances d'affichage
- Habillage de texte
  ##La couleur
- Couleur et nuancier
- Dégradés progressifs

##Étapes

1. Préparation et vérification des sources ≃ 5%
2. Choix du format du document, création des couleurs et choix des polices ≃ 5%
3. Création des Gabarits, repères et albums ≃ 15%
4. Préparation et création des styles de texte ≃ 30%
   Produire
5. Importation des textes et des images, et application des styles ≃ 15%
6. Positionnements manuels et finitions typographiques
   ≃ 10%
7. Correction et vérification de la mise en page ≃ 15%
8. Exportation en PDF et réglages finaux pour l'impression ≃ 5%

![Olafur](/assets/Olafur_Eliasson_1/autre_01.jpg)

##Olafur Eliasson

Mettez en page l'interview d'Olafur Eliasson que vous trouverez dans l'archive ci-dessous. Une série d'images sont également disponibles pour illustrer votre travail.

- [Matière première](/assets/Olafur_Eliasson.zip)

Ce travail est à réaliser au cours et est à déposer dans vos dossiers dropbox à la fin du prochain cours.

####Pour ceci:

- Créez un document de minimum **6 pages** en vis-à-vis (ou pas).
- Fond perdu **4mm**.
- Utilisez (importez ou copiez) le texte des fichiers contenus dans l'archive. trouvez-en d'autres supplémentaires si nécessaire.
- Pas de gabarit pour la couveture
- Utilisez **au moins un gabarit** (par ex. gabarit A)
- des **blocs de texte chaînés**
- Les styles se font **exclusivement via les styles de paragraphe et de caractère** (titre, introduction, auteur, légende, qui-parle, ...) Création d'au moins 4 styles de paragraphe donc)
- sauvez régulièrement votre travail
  (Autoriser la réorganisation des pages - menu pages)
