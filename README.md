#Fold/Unfold : Horaires des remises

Vous trouverez ci-dessous les horaires de passage des remises finales qui auront lieu **à l'Abbaye, Salle art numérique 1 ou 2**. Si vous préférez une autre heure que celle indiquée, merci de vous arranger pour permuter avec un.e collègue et de m'informer de la modification. Veillez à relire attentivement le briefing du travail sur digitalab.be.

Dans tous les cas, soyez-là 5 minutes avant l'heure prévue. Notez qu'il y a toujours un risque que ces horaires soient modifiés. **Les horaires affichés sur notre site ( b1.artsnum.be ) seront alors mis à jour**.

##Remises finales du vendredi 14 juin

+ **Emma** 10:00
+ **Dembaye** 10:15
+ **Cynthia** 10:30
+ **Aubry** 10:45
+ **Thomas** 11:00
+ **Louise** 11:15
+ **Matéo** 11:45
+ **Sophie** 12:00
+ **Rébecca** 12:15
+ **Hadrien** 12:30
+ **Ximena** 12:45
+ **Claire** 13:00

**IMPORTANT** : Les étudiants non présents dans cette liste et désirant passer les évaluations de juin pour ce cours peuvent me contacter directement par mail.