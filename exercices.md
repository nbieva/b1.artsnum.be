----

#Indesign <div class="badges"><span class="remise">Au cours et à remettre</span></div>

Mettez à jour votre fichier INDD en ajoutant:

+ Au moins **trois images**, dont deux avec un habillage de texte.
+ Créez un **assemblage Indesign** (vérifiez ensuite que tous les fichiers y sont), compressez-le et déposez-le dans votre dossier Dropbox.

#Indesign <div class="badges"><span class="remise">17 mai</span> <span class="remise current">En cours</span></div>

Sur base de ce qui a été vu au dernier cours (et du document ci-dessous), créez un document Indesign (ou Scribus) comprenant:

+ Au moins une planche (2 pages en vis-à-vis)
+ Un gabarit utilisé (vous devez y déposer au moins un élément, et appliquer ce gabarit à au moins une page)
+ Plusieurs blocs de textes, dont au moins 2 sont chaînés entre eux.
+ Le bloc principal doit s'afficher en 3 colonnes.
+ Les styles de paragraphes suivants: Texte courant (Paragraphe standard), Titre, Introduction, Petit titre (dans le texte courant, utilisé plusieurs fois), et éventuellement une lettrine pour ceux qui s'en souviennent.
+ Un style de caractère

Faites en sorte de vous rapprocher le plus possible du document suivant :

![Voir le PDF](assets/indesign1.png)

Le document Indesign doit être déposé dans le fichier Dropbox AVANT le prochain cours.

#Masques de fusion <div class="badges"><span class="remise">26 avril</span> </div>

Tout ce qui concerne le travail à rendre est repris sur [la page du dernier cours](https://b1.artsnum.be/bitmap/masques.html).

#Sélections et calques <div class="badges"><span class="remise">5 avril</span></div>

A partir d'un des tableaux de René Magritte (ou de votre artiste préféré). Séléctionner une "ouverture" pour remplacer ce qu'elle contient par un contenu de votre choix, idéalement en dialogue avec l'image. Le choix du bon outil de sélection est ici primordial.
Votre document final doit être un document Photoshop (PSD), avec vos calques conservés, de 1500px sur 1000px (ou de 1000px sur 1500px)

La qualité des images sera un critère d'évaluation pour ceci.

[Magritte en images](https://www.google.be/search?q=magritte&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjsyf-YqaLeAhXNLVAKHemMBNwQ_AUIDigB&biw=1393&bih=779)

![René Magritte](/assets/magritte.png)

####Pour vous aider

+ https://fr.wikibooks.org/wiki/Adobe_Photoshop/Outils_de_s%C3%A9lection
+ http://paulbourke.net/dataformats/bitmaps/
+ http://neocha.com/magazine/hisham-akira-bharoocha/
+ https://helpx.adobe.com/be_fr/photoshop/how-to/selection-tools-basics.html
+ http://arts-numeriques.codedrops.net/Bit-octet-kilo-octets

------

# Josh Brill <div class="badges"><span class="remise">8 mars</span></div>

Sur base d'un travail de [Josh Brill](https://www.joshbrill.co/), réalisez un animal (ou un végétal. Vous pouvez utiliser l'image sur [cette page](vectoriel/pathfinder-et-masques.md) en utilisant:

+ Les **formes simples**
+ Les **groupes** et les **alignements**
+ Le dessin à la **plume**
+ La palette **pathfinder**
+ Les **masques**
+ Le **texte**

Choisissez ensuite deux formes dont l'une sera remplie d'un **dégradé de couleur**, l'autre d'une image (avec un **masque d'écrêtage**). Eventuellement une troisième remplie d'un **motif** que vous créez. Cherchez une police semblable sur Google Fonts.

Idéalement, vous devriez pouvoir dissocier chaque forme des autres (plus ou moins), à la manière d'un puzzle (voir les images ci-dessous).

Voir ci-dessous:

![Bird exo](/assets/brill.png)

![Bird exo](/assets/bird-exo.jpg)

![Josh](/assets/birds.jpg))

# Canabec <div class="badges"><span class="remise">1er mars</span></div>

Réalisez, comme vu au cours, les drapeaux canadien et québéquois en utilisant la plume.

+ Gardez un oeil sur vos calques pour garder votre fichier le plus "propre" possible.
+ Alignez et groupez vos éléments

Le fichier Illustrator est à déposer dans votre dossier personnel Dropbox avant le prochain cours. Vérifiez au passage que les précédent y soient également.

![Canada flag](/assets/canada.png)
![Fleur de lys](/assets/750px-Fleur_de_lys_du_quebec.png)
![Québec flag](/assets/quebec.png)

# Anni Albers <div class="badges"><span class="remise">22 février</span></div>

Sur base du travail d'[Anni Albers](https://www.1stdibs.com/introspective-magazine/anni-albers-touching-vision/), créez votre propre pattern sur base de formes géométriques parfaitement alignées et paramétrées. Vous pouvez également partir du travail d'un.e autre artiste qui correspond à l'objectif de cet exercice: travail sur les alignements paramétrés et sur les groupes.
+ Format A4 (297x210mm)
+ 5mm de bord perdu si nécessaire (ce devrait l'être..)
+ Mode colorimétrique RVB

Votre travail doit être déposé dans votre dossier partagé Dropbox avant la date reprise ci-dessus.

Liens:
+ https://www.1stdibs.com/introspective-magazine/anni-albers-touching-vision/
+ [Sur Google images](https://www.google.be/search?q=anni+albers&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjuyNvSxtvdAhXELlAKHVE2CnwQ_AUICigB&biw=1920&bih=947)

# Formes simples <div class="badges"><span class="remise">15 février</span></div>

1. Dans l'image ci-dessous (Matt Mullican), **choisissez 3 signes** que vous reproduisez à l’aide des formes simples vues au cours. **Inventez-en un quatrième**. N'utilisez que du noir et du blanc. Votre document Illustrator doit donc comprendre **4 plans de travail de 10x10cm** \(avec un fond perdu si nécessaire\).

2. Configuration de votre dossier Dropbox et transfert de votre fichier Illustrator dans votre dossier personnel (nommez vos fichiers comme suit **nom-prenom-atelier-mullican.ai**)

![](/assets/mullican1.jpg)