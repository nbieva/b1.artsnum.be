![](/images/libres.png)

### Définition {#definition}

Un logiciel libre est un logiciel qui est distribué selon une[licence libre](http://www.aful.org/ressources/licences-libres). Précisément, ce sont les licences libres qui définissent les logiciels comme tels.

Plus concrètement et de manière un peu simplifiée[\[Note\]](https://aful.org/ressources/logiciel-libre/#precisions-modification-diffusion), cela se matérialise par le fait qu’un logiciel libre est un logiciel qui peut être utilisé, modifié et redistribué sans restriction par la personne à qui il a été distribué. Un tel logiciel est ainsi susceptible d’être soumis à étude, critique et correction. Cette caractéristique confère aux logiciels libres une certaine fiabilité et réactivité.

Mozilla Firefox, Mozilla Thunderbird, OpenOffice.org et VLC sont des exemples de logiciels libres célèbres. Si vous avez déjà utilisé un de ces logiciels, vous avez donc déjà utilisé un logiciel libre !

### Logiciels libres et Open Source {#logiciel-libre-opensource}

À l’AFUL nous faisons peu de distinction entre « logiciels libres » et « Open Source » car en pratique les licences définies comme libres par la_Free Software Foundation_\(FSF\) et l’_Open Source Initiative_\(OSI\), cf.[licences libres](http://www.aful.org/ressources/licences-libres), sont identiques à quelques cas anecdotiques près.

Si dans la pratique logiciel libre et Open Source désignent le même objet, ils le considèrent respectivement de points de vue différents :

* la défense de la liberté des utilisateurs et des clients
* l’efficacité technologique et commerciale pour les clients, les prestataires de services et les éditeurs

Nous préférons cependant employer les mots_logiciels libres_car, plus clairs, ils évitent les glissements sémantiques commemais si, c’est ouvert, puisque vous pouvez lire le codeutilisés à l’envie par certains acteurs comme Microsoft.

Le terme_FLOSS_pourFree/Libre/Open-Source Software, plus récemment introduit, est une manière d’utiliser un terme consensuel.

### Questions / Réponses

**Un logiciel libre est-il gratuit ?**Un logiciel libre n’est pas forcément gratuit. L’ambiguïté provient de l’expression d’origine,_free software_, puisqu’en anglais_free_signifie aussi bien libre que gratuit. Dans la pratique, nombre de logiciels libres se trouvent gratuitement sur certains sites web. Des versions payantes, mais souvent très bon marché, sont commercialisées par des entreprises sous forme de CD-ROM ou DVD-ROM, avec notice complète, et contrat d’assistance à l’installation ou de maintenance. Par exemple les sociétés[Canonical](http://www.canonical.com/)\(Royaume-Uni\),[RedHat](http://www.redhat.com/)\(USA\),[Suse](https://www.suse.com/)\(Allemagne\) distribuent ainsi différentes versions du système GNU-Linux.

**Qu’est-ce qui différencie un logiciel propriétaire d’un logiciel libre ?**La grande majorité des logiciels vendus dans le commerce sont des logiciels propriétaires, qui sont distribués en version « exécutable », alors que les logiciels libres sont fournis avec leur « code source ». Source ? Exécutable ? Un petit détour par une analogie musicale permet d’éclairer ces termes. On peut considérer le code source d’un logiciel comme la partition de celui-ci, et le code exécutable comme sa version enregistrée. Une partition peut être jouée sur un piano, une flûte ou par l’orchestre philharmonique de Berlin. En revanche un enregistrement pressé sur disque ne permet pas de modifier la musique, de changer d’instrument ou de moduler l’interprétation.

Le passage de l’un à l’autre s’opère par traduction du code source \(lu et écrit par l’homme\) en code exécutable \(que seul l’ordinateur comprend\). Les logiciels libres sont distribués sous ces deux formes, tandis que Microsoft ou Adobe ne vendent que le code « exécutable » et cachent le reste.

**Qui crée des logiciels libres ?**Toute personne \(informaticiens, graphistes, musiciens, traducteurs, relecteurs, testeurs, etc.\) désireuse de bénéficier de créations collectives et/ou souhaitant maximiser la diffusion de ses œuvres \(logiciels, textes, images, vidéos, musiques\).

A la tête de chaque projet de logiciel libre il y a une structure plus ou moins formelle qui est composée de simples particuliers et/ou d’entreprises.

Par exemple, la FSF, dirigée par Richard Stallman, produit et/ou organise le développement de logiciels libres. Ainsi le projet GNU \(dont le logo est, bien sûr, un gnou\) de la FSF a joué un rôle déterminant dans la création de Linux \(dont le logo est un manchot\).

---

Note : Il y a des subtilités en ce qui concerne la modification et la diffusion des logiciels, et particulièrement des logiciels libres, qui sont présentées en détail dans le[document de présentation des licences libres](http://www.aful.org/ressources/licences-libres)et dans[le dossier sur les modèles économiques liés aux logiciels libres](http://www.aful.org/professionnels/modeles-economiques-ll).

Source:[**Association Francophone des Utilisateurs de Logiciels Libres**](https://aful.org/)